//
//  FreestarIdentity.h
//  FSTRAdsIdentity
//
//  Created by Dean Chang on 9/11/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class FSTRAdsIdentityInfo;

@protocol FreestarIdentityDelegate <NSObject>
- (void)identityDidInitialize;
- (void)identityFailedToInitializeWithError:(NSError*)error;

@end

typedef NS_ENUM(NSUInteger, FreestarIdentityProvider) {
    FreestarIdentityProviderLiveRamp = 0
};

@interface FreestarIdentity : NSObject

+ (instancetype)shared;

@property(nonatomic, assign, readonly, getter=isInitialized) BOOL initialized;
@property(nonatomic, weak) id<FreestarIdentityDelegate> delegate;
@property(nonatomic, strong, readonly) NSArray<FSTRAdsIdentityInfo*>  *identityPartners;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSString *custom;

- (nullable NSString*)userIdentifier;

- (void)initializeWithIdentityProviders:(NSArray<FSTRAdsIdentityInfo*> *)identityProviders
                consentForNoLegislation:(BOOL)consentForNoLegislation;

- (void)retrieveIdentityEnvelopForProvider:(FreestarIdentityProvider)provider
                                completion:(void (^ _Nonnull)(NSString * _Nullable, NSError * _Nullable))completion;

- (void)retrieveEnvelopeWithPhoneIdentifier:(NSString*)identifier
                                   provider:(FreestarIdentityProvider)provider
                                 completion:(void (^ _Nonnull)(NSString * _Nullable, NSError * _Nullable))completion;
- (void)retrieveEnvelopeWithEmailIdentifier:(NSString*)identifier
                                   provider:(FreestarIdentityProvider)provider
                                 completion:(void (^ _Nonnull)(NSString * _Nullable, NSError * _Nullable))completion;
- (void)retrieveEnvelopeWithCustomIdentifier:(NSString*)identifier
                                    provider:(FreestarIdentityProvider)provider
                                  completion:(void (^ _Nonnull)(NSString * _Nullable, NSError * _Nullable))completion;

@end

NS_ASSUME_NONNULL_END
