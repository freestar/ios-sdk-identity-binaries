//
//  FSTRAdsIdentityInfo.h
//  FSTRAdsIdentity
//
//  Created by Dean Chang on 9/12/23.
//

#import <Foundation/Foundation.h>

#define IdentityProviderAPIKey @"apiKey"
#define IdentityProviderAppId @"appId"

typedef NS_ENUM(NSUInteger, FreestarIdentityProvider);

NS_ASSUME_NONNULL_BEGIN

@interface FSTRAdsIdentityInfo : NSObject

@property(nonatomic, assign, readonly) FreestarIdentityProvider provider;
@property(nonatomic, strong, readonly) NSDictionary *extrasInfo;
@property(nonatomic, assign, readonly, getter=isEnabled) BOOL enabled;
@property(nonatomic, strong, readonly) NSString *identityEnvelope;
@property(nonatomic, assign, getter=isTestModeEnabled) BOOL testModeEnabled;

@property(nonatomic, strong, readonly) NSString *apiKey;
@property(nonatomic, strong, readonly) NSString *appId;

- (instancetype)initWithIdentityProvider:(FreestarIdentityProvider)provider
                              extrasInfo:(NSDictionary*)extrasInfo;

@end

NS_ASSUME_NONNULL_END
