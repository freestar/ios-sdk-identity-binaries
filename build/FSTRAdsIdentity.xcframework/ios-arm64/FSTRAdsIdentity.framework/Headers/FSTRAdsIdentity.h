//
//  FSTRAdsIdentity.h
//  FSTRAdsIdentity
//
//  Created by Dean Chang on 9/11/23.
//

#import <Foundation/Foundation.h>

//! Project version number for FSTRAdsIdentity.
FOUNDATION_EXPORT double FSTRAdsIdentityVersionNumber;

//! Project version string for FSTRAdsIdentity.
FOUNDATION_EXPORT const unsigned char FSTRAdsIdentityVersionString[];

#import <FSTRAdsIdentity/FreestarIdentity.h>
#import <FSTRAdsIdentity/FSTRAdsIdentityInfo.h>



