Pod::Spec.new do |spec|
  spec.name                = "FSTRAdsIdentity"
  spec.version             = "1.0.0"
  spec.author              = 'Freestar'
  spec.license             =  { :type => 'Apache2.0', :file => 'LICENSE' }
  spec.homepage            = 'http://www.freestar.com'
  spec.summary             = 'The Freestar Ads Identity SDK'
  spec.platform            = :ios, '12.0'
  spec.swift_version       = '5.0'

  spec.vendored_frameworks = 'build/FSTRAdsIdentity.xcframework'
  spec.dependency "LRAtsSDK", "2.2.0"

  spec.source              = {
      :git => "https://gitlab.com/freestar/ios-sdk-identity-binaries.git",
      :tag => "1.0.0"
  }

end
